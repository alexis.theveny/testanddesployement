import unittest
import main


class TestUser(unittest.TestCase):
    def test_User(self):
        # Tout OK
        user = main.User('alexis.theveny@livecampus.tech', 'Alexis', 'Theveny',
                         21)
        # Invalid email
        user1 = main.User('alexis.thevenylivecampus.tech', 'Alexis', 'Theveny',
                          21)
        # Empty Firstname
        user2 = main.User('alexis.theveny@livecampus.tech', '', 'Theveny', 21)
      
        # Empty Lastname
        user3 = main.User('alexis.theveny@livecampus.tech', 'Alexis', '', 21)
      
        # Under 13 yo
        user4 = main.User('alexis.theveny@livecampus.tech', 'Alexis',
                          'Theveny', 12)
        # Everything wrong
        user5 = main.User('alexis.thevenylivecampus.tech', '', '', 12)

        # Can add more possibilities of test...

        self.assertTrue(user.isValid())
        self.assertFalse(user1.isValid())
        self.assertFalse(user2.isValid())
        self.assertFalse(user3.isValid())
        self.assertFalse(user4.isValid())
        self.assertFalse(user5.isValid())


if __name__ == '__main__':
    unittest.main()
